#!/usr/bin/env python3

import argparse
import os.path
import platform
import shutil
import subprocess

script_dir = os.path.dirname(os.path.realpath(__file__))

parser = argparse.ArgumentParser(description="Build project")
parser.add_argument(
    "--build-type",
    choices=["Release", "Debug"],
    dest="build_type",
    default="Debug",
    help="The build type, Release or Debug")

args = parser.parse_args()

lib_output = os.path.join(script_dir, "output", args.build_type)

cmake_args = [
    # CMake generator
    "-G", "Xcode",
    # CMake config
    "-DCMAKE_BUILD_TYPE={}".format(args.build_type),
    "-DCMAKE_INSTALL_PREFIX='{}'".format(lib_output),
    "--no-warn-unused-cli",
    # Platform config
    # Library config
    "-DBUILD_SHARED_LIBS=FALSE",
    "-DASSIMP_BUILD_TESTS=FALSE",
    "-DASSIMP_BUILD_ASSIMP_TOOLS=FALSE",
    "-DASSIMP_BUILD_ALL_IMPORTERS_BY_DEFAULT=FALSE",
    "-DASSIMP_BUILD_OBJ_IMPORTER=TRUE",
    "-DASSIMP_BUILD_PLY_IMPORTER=TRUE",
    "-DASSIMP_BUILD_FBX_IMPORTER=TRUE"
]

if __name__ == "__main__":
    cmake_path = shutil.which("cmake")
    source_dir = os.path.join(script_dir, "..", "..", "assimp")
    # Generate project
    generate_command = [cmake_path, source_dir] + cmake_args
    subprocess.run(generate_command, cwd=script_dir, check=True)
    # Execute build
    build_command = [cmake_path, "--build", script_dir, "--target", "install",
                     "--config", args.build_type]
    subprocess.run(build_command, cwd=script_dir, check=True)
    # Copy include to library output
    output_file_path = os.path.join(lib_output, "include", "config.h")
    shutil.move(os.path.join(lib_output, "include", "assimp", "config.h"),
                output_file_path)
